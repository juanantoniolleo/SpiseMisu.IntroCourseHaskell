#!/usr/bin/env stack
{- stack
   --resolver lts-12.0
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --
-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

class Monad m => ConsoleOutM m where
  consoleOut :: String -> m ()

--------------------------------------------------------------------------------

instance ConsoleOutM IO where
  consoleOut = putStrLn

--------------------------------------------------------------------------------

granulated
  :: ConsoleOutM m
  => m ()

main
  :: IO ()

--------------------------------------------------------------------------------

granulated =
  consoleOut "We can only write to the console"

main =
  granulated
