#!/bin/bash

clear

./clear.bash

# reverse
ghc -Wall -Werror -O2 --make Main.hs -o reverse

# run
echo -n "Some Text" | ./reverse && echo

# clean
find . -name '*.hi' -delete
find . -name '*.o'  -delete
