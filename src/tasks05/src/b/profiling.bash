#!/bin/bash

clear

./clear.bash

# acc_count (+ profiling)
ghc -prof -fprof-auto -rtsopts -Wall -Werror -O2 --make Main.hs -o acc_count

# run and generate memory profile
#cat ./acc_count | ./acc_count +RTS -h && echo
cat ~/downloads/debian-9.5.0-amd64-netinst.iso | ./acc_count +RTS -h && echo

# create a graph of memory profile
hp2ps -c acc_count.hp

# clean
find . -name '*.hi' -delete
find . -name '*.o'  -delete
