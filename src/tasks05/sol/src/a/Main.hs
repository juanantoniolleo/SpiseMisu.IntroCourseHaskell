module Main (main) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy       as LBS
import qualified Data.ByteString.Lazy.Char8 as L8

--------------------------------------------------------------------------------

count :: LBS.ByteString -> Integer
count =
  aux
  where
  aux bs
    | LBS.null bs = 0
    | otherwise   = 1 + (aux $ LBS.tail bs)

main
  :: IO ()

--------------------------------------------------------------------------------

main =
  LBS.interact $ L8.pack . show . count
