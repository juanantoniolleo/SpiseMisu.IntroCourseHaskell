#!/bin/bash

lib="$1"

stack \
    --resolver lts-12.0 \
    --install-ghc \
    new "$lib" simple-library
