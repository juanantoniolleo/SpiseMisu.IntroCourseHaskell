Intro Course in Haskell
=======================

Come to an Introductory Course in Haskell - a standardized, general-purpose
purely functional programming language, with non-strict semantics and strong
static typing. Haskell is used widely in academia but also in
[industry][industry].

[industry]: https://wiki.haskell.org/Haskell_in_industry

### Target group

Given that the language has a clear separation between code that produces
side-effects (mutating objects or outputting to an IO device) and pure (always
evaluates to the same output value given the same input) enforced by the
compiler. This will allow to make better software applications by ensuring that
design constraints are maintained through the whole application.

This differs from most of the current tooling that is used in the industry as
today, where this responsibility is placed on the shoulders of the developers,
which is not error-prone, and can lead to non-desired behavior that is very
difficult to catch when testing.

As from the end of May, due to the upcoming EU GDPR (General Data Protection
Regulation), not having control over processes handling sensitive personal data,
can have fatal consequences.

Therefore, this course is aimed for people that want to get a good start when
developing Haskell applications and packages.

### Prerequisites:

Participants must have a minimum knowledge of other programming
languages. Knowledge of other functional programming languages will be an
advantage, but not crucial.

### Program:

* A few basic concepts to get started

* The Haskell Tool Stack (scripts, applications and packages)

* Domain modeling with Types

* A few high-order functions that will be used again and again

* Testing with Hspec and QuickCheck

* Profiling to avoid stack overflows and space leaks

* Safe applications and packages

* Isolating and granulating side-effects

### Teacher:

Ramón Soto Mathiesen is a computer scientist and independent IT consultant at
SPISE MISU ApS, as well as one of the driving forces behind Functional
Copenhageners Meetup Group.

### Comments:

#### First:

As there is little more than a week and a half to this introductory Haskell
course, I think it would be ideal to already install the tooling that we will
need. We will expect that you bring your own device, preferable a laptop. Since
we assume that you will run different Operating Systems, here is a few things
you will need to install based on your OS choice:

* The Haskell Tool Stack (*):

    -- https://docs.haskellstack.org/en/stable/install_and_upgrade/ 
    
* IDE Intellisense (Intero):

    -- Emacs: http://commercialhaskell.github.io/intero/
	
    -- Vi (Neovim): https://github.com/parsonsmatt/intero-neovim
	
    -- Sublime: https://github.com/dariusf/sublime-intero#installation
	
    -- VSCode: https://marketplace.visualstudio.com/items?itemName=Vans.haskero 
    
If you have any question, please don't hesitate to write them here.

(*) - As a Windows user, it's recommended that you first install Ubuntus
Terminal (https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6) and from there
that you install Stack (with either wget/curl).
